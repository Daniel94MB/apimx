var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path')

var requestjson = require('request-json')

var bodyparser = require('body-parser')
app.use(bodyparser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

var movimientosJSON = require('./movimientosv2.json')

var urlClientes = "https://api.mlab.com/api/1/databases/dmiranda/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var clienteMLab = requestjson.createClient(urlClientes)

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/dmiranda/collections/";
var apiKey = "?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlabRaiz;

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'))
})

app.post('/', function(req, res) {
  res.send("Hemos recibido su peticion post")
})

app.put('/', function(req, res) {
  res.send("Hemos recibido su peticion put cambiada")
})

app.delete('/', function(req, res) {
  res.send("Hemos recibido su peticion delete")
})

app.get('/Clientes/:idcliente', function(req, res) {
  res.send("Aqui tiene al cliente número: " + req.params.idcliente);
})

app.get('/v1/Movimientos', function(req, res) {
  res.sendfile('movimientosv1.json')
})

app.get('/v2/Movimientos', function(req, res) {
  res.json(movimientosJSON)
})

app.get('/v2/Movimientos/:idcliente', function(req, res) {
  res.send(movimientosJSON[req.params.idcliente-1]);
})

app.get('/v2/Movimientosquery', function(req, res) {
  res.send(req.query)
})

app.post('/v2/Movimientos', function(req, res) {
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
  res.send("Movimiento dado de alta")
})

app.get('/Clientes', function(req, res) {
  clienteMLab.get('', function(err, resM, body) {
    if (!err) {
      res.send(body);
    } else {
      console.log(body);
    }
  })
})

app.post('/Clientes', function(req, res) {
  clienteMLab.post('', req.body, function(err, resM, body) {
      res.send(body);
  })
})


app.post('/Login',function(req,res) {
  //res.set("Access-Control-Allow-Headers","Content-Type");
  var email = req.body.email
  var password = req.body.password

  var query = 'q={"email":"'+email+'","password":"'+password+'"}'
  console.log(query)

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Usuarios" + apiKey + "&" + query)
  console.log(urlMlabRaiz + "Usuarios" + apiKey + "&" + query);

  clienteMlabRaiz.get('',function(err,resM,body){
    if (!err) {
      if (body.length == 1) {
        res.status(200).send("Usuario loggeado");
      }
      else{
        res.status(404).send("Usuario no encontrado");
      }
    }
  });
})
